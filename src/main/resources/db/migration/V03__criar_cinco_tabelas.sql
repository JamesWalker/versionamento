CREATE TABLE fornecedor(
	fornecedor_id BIGINT PRIMARY KEY NOT NULL,
	nome VARCHAR(50) NOT NULL,
	campo_1 varchar (50),
    campo_2 varchar (50),
    campo_3 varchar (50),
    campo_4 varchar (50),
    campo_5 varchar (50)
)  ;

CREATE TABLE financeiro(
	financeiro_id BIGINT PRIMARY KEY NOT NULL,
	nome VARCHAR(50) NOT NULL,    
	campo_1 varchar (50),
    campo_2 varchar (50),
    campo_3 varchar (50),
    campo_4 varchar (50),
    campo_5 varchar (50)
)  ;

CREATE TABLE relatorio(
	relatorio_id BIGINT PRIMARY KEY NOT NULL  ,
	nome VARCHAR(50) NOT NULL,
	campo_1 varchar (50),
    campo_2 varchar (50),
    campo_3 varchar (50),
    campo_4 varchar (50),
    campo_5 varchar (50)
)  ;

CREATE TABLE movimento_estoque(
	movimentoestoque_id BIGINT PRIMARY KEY NOT NULL  ,
	nome VARCHAR(50) NOT NULL,
	campo_1 varchar (50),
    campo_2 varchar (50),
    campo_3 varchar (50),
    campo_4 varchar (50),
    campo_5 varchar (50)
)  ;

CREATE TABLE compras(
	compras_id BIGINT PRIMARY KEY NOT NULL  ,
	nome VARCHAR(50) NOT NULL,
	campo_1 varchar (50),
    campo_2 varchar (50),
    campo_3 varchar (50),
    campo_4 varchar (50),
    campo_5 varchar (50)
)  ;