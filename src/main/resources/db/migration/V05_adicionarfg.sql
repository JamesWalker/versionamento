ALTER TABLE marca ADD CONSTRAINT produto_id FOREIGN KEY (produto_id) REFERENCES produto (produto_id);

ALTER TABLE produto ADD CONSTRAINT marca_id FOREIGN KEY (marca_id) REFERENCES marca (marca_id);

ALTER TABLE produto ADD CONSTRAINT fornecedor_id FOREIGN KEY (fornecedor_id) REFERENCES fornecedor (fornecedor_id);

ALTER TABLE fornecedor ADD CONSTRAINT produto_id FOREIGN KEY (produto_id) REFERENCES produto (produto_id);

ALTER TABLE relatorio ADD CONSTRAINT movimentoestoque_id FOREIGN KEY (movimentoestoque_id) REFERENCES movimentoestoque (movimentoestoque_id);

ALTER TABLE movimento_estoque ADD CONSTRAINT relatorio_id FOREIGN KEY (relatorio_id) REFERENCES relatorio (relatorio_id);



