//
//
//
//
//

public class ProdutoValidation {
    public static void main(String[] args) {
  
    //  Produto produto = new Compra(1, "Teste nome", "Teste", "Teste", "Teste", "Teste", "Teste", "Teste");
  
    ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    Validator validator = factory.getValidator ();
  
    Set<ConstraintViolation<Produto>> constraintViolations =
    validator.validate( produto);
  
    for (ConstraintViolation error: constraintViolations) {
        String msgError = error.getMessage();
        System.out.println (msgError);
    }
  }
  }