package br.edu.unidep.apiseguranca.apiseguranca.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Table(name = "financeiro")
public class Financeiro {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "financeiro_id")
	private Long financeiro_id;
	
	@NotNull
	@Size(min = 5, max = 50)
	@Column(name = "nome")
	private String nome;

	@Size(min = 5, max = 50)
	@Column(name = "campo_1")
	private String campo_1;
	
	@Size(min = 5, max = 50)
	@Column(name = "campo_2")
	private String campo_2;

	@Size(min = 5, max = 50)
	@Column(name = "campo_3")
	private String campo_3;

	@Size(min = 5, max = 50)
	@Column(name = "campo_4")
	private String campo_4;

	@Size(min = 5, max = 50)
	@Column(name = "campo_5")
	private String campo_5;
	
	
	
	public Long getCodigo() {
		return financeiro_id;
	}

	public void setCodigo(Long financeiro_id) {
		this.financeiro_id = financeiro_id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCampo1() {
		return campo_1;
	}

	public void setCampo1(String campo_1) {
		this.campo_1 = campo_1;
	}

	public String getCampo2() {
		return campo_2;
	}

	public void setCampo2(String campo_2) {
		this.campo_2 = campo_2;
	}

	public String getCampo3() {
		return campo_3;
	}

	public void setCampo3(String campo_3) {
		this.campo_3 = campo_3;
	}

	public String getCampo4() {
		return campo_4;
	}

	public void setCampo4(String campo_4) {
		this.campo_4 = campo_4;
	}
	public String getCampo5() {
		return campo_5;
	}

	public void setCampo5(String campo_5) {
		this.campo_5 = campo_5;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((financeiro_id == null) ? 0 : financeiro_id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Financeiro other = (Financeiro) obj;
		if (financeiro_id == null) {
			if (other.financeiro_id != null)
				return false;
		} else if (!financeiro_id.equals(other.financeiro_id))
			return false;
		return true;
	}
	
}
