package br.edu.unidep.apiseguranca.apiseguranca.rest;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;


import br.edu.unidep.apiseguranca.apiseguranca.model.MovimentoEstoque;
import br.edu.unidep.apiseguranca.apiseguranca.repository.MovimentoEstoqueRepository;

@RestController
@RequestMapping("/movimentoestoque")
public class MovimentoEstoqueRest {
	
	@Autowired
	MovimentoEstoqueRepository repositorio;
	
	@GetMapping("/ok")
	public String ok() {
		return "ok";
	}

	@GetMapping
	public List<MovimentoEstoque> listar() {
		return repositorio.findAll();
	}
	
	@PostMapping
	public ResponseEntity<MovimentoEstoque> criar(@Valid @RequestBody MovimentoEstoque movimentoestoque,
			HttpServletResponse response) {
		
		MovimentoEstoque movimentoestoqueSalva = repositorio.save(movimentoestoque);
		
		URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().
				path("/{movimentoestoque_id}").buildAndExpand(
						movimentoestoqueSalva.getCodigo()).toUri();
						
		response.setHeader("Location", uri.toASCIIString());
		
		return ResponseEntity.created(uri).body(movimentoestoqueSalva);
		
	}
	
	@GetMapping("/{movimentoestoque_id}")
	public ResponseEntity<MovimentoEstoque> buscarPeloCodigo(
			@PathVariable Long movimentoestoque_id) {
		
		Optional<MovimentoEstoque> movimentoestoque = repositorio.findById(movimentoestoque_id);
		
		return ResponseEntity.ok(movimentoestoque.get());
	}
	
	@DeleteMapping("/{movimentoestoque_id}")
	public void remover(@PathVariable Long movimentoestoque_id) {
		Optional<MovimentoEstoque> movimentoestoque = repositorio.findById(movimentoestoque_id);
		if (movimentoestoque.isPresent()) {
			repositorio.deleteById(movimentoestoque_id);
		}
	}
}
