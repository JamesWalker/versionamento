package br.edu.unidep.apiseguranca.apiseguranca.enums;

import br.edu.unidep.apiseguranca.apiseguranca.model.Financeiro;
import br.edu.unidep.apiseguranca.apiseguranca.repository.FinanceiroRepository;
import org.sprigframework.beans.factoy.annotation.Autowired;
import org.sprigframework.http.HttpStatus;
import org.sprigframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/financeiro")
public class FinanceiroController {

    @Autowired
    private FinanceiroData financeiroData;

    @GetMapping("{financeiro_id}")
    private Financeiro findById(@PathVariable("financeiro_id")Long financeiro_id) {
        return financeiroData.findById(financeiro_id),orElse(other: null);
    }

    @GetMapping
    private List<Financeiro> findAll {
        return financeiroData.findAll();
    }

    @PostMapping
    @RespondeStatus(HttpStatus.CREATED)
    private Financeiro save(@ResquestBody Financeiro financeiro){
        return financeiroData.save(financeiro);
    }

    @DeleteMapping
    @RespondeStatus(HttpStatus.NO_CONTENT)
    private void delete(@RequestParam("financeiro_id") Long financeiro_id){
        financeiroData.deleteById(financeiro_id);
    }
}