package br.edu.unidep.apiseguranca.apiseguranca.enums;

import br.edu.unidep.apiseguranca.apiseguranca.model.Relatorio;
import br.edu.unidep.apiseguranca.apiseguranca.repository.RelatorioRepository;
import org.sprigframework.beans.factoy.annotation.Autowired;
import org.sprigframework.http.HttpStatus;
import org.sprigframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/relatorio")
public class RelatorioController {

    @Autowired
    private RelatorioData relatorioData;

    @GetMapping("{relatorio_id}")
    private Relatorio findById(@PathVariable("relatorio_id")Long relatorio_id) {
        return relatorioData.findById(relatorio_id),orElse(other: null);
    }

    @GetMapping
    private List<Relatorio> findAll {
        return relatorioData.findAll();
    }

    @PostMapping
    @RespondeStatus(HttpStatus.CREATED)
    private Relatorio save(@ResquestBody Relatorio relatorio){
        return relatorioData.save(relatorio);
    }

    @DeleteMapping
    @RespondeStatus(HttpStatus.NO_CONTENT)
    private void delete(@RequestParam("relatorio_id") Long relatorio_id){
        relatorioData.deleteById(relatorio_id);
    }
}