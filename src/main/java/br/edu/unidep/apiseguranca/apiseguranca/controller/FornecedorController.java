package br.edu.unidep.apiseguranca.apiseguranca.enums;

import br.edu.unidep.apiseguranca.apiseguranca.model.Fornecedor;
import br.edu.unidep.apiseguranca.apiseguranca.repository.FornecedorRepository;
import org.sprigframework.beans.factoy.annotation.Autowired;
import org.sprigframework.http.HttpStatus;
import org.sprigframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/fornecedor")
public class FornecedorController {

    @Autowired
    private FornecedorData fornecedorData;

    @GetMapping("{fornecedor_id}")
    private Fornecedor findById(@PathVariable("fornecedor_id")Long fornecedor_id) {
        return fornecedorData.findById(fornecedor_id),orElse(other: null);
    }

    @GetMapping
    private List<Fornecedor> findAll {
        return fornecedorData.findAll();
    }

    @PostMapping
    @RespondeStatus(HttpStatus.CREATED)
    private Fornecedor save(@ResquestBody Fornecedor fornecedor){
        return fornecedorData.save(fornecedor);
    }

    @DeleteMapping
    @RespondeStatus(HttpStatus.NO_CONTENT)
    private void delete(@RequestParam("fornecedor_id") Long fornecedor_id){
        fornecedorData.deleteById(fornecedor_id);
    }
}