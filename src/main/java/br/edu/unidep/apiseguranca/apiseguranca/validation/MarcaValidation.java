//
//
//
//
//

public class MarcaValidation {
    public static void main(String[] args) {
  
    //  Marca marca = new Compra(1, "Teste nome", "Teste", "Teste", "Teste", "Teste", "Teste", "Teste");
  
    ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    Validator validator = factory.getValidator ();
  
    Set<ConstraintViolation<Marca>> constraintViolations =
    validator.validate( marca);
  
    for (ConstraintViolation error: constraintViolations) {
        String msgError = error.getMessage();
        System.out.println (msgError);
    }
  }
  }