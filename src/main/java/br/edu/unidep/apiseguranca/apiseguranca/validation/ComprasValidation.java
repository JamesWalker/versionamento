//
//
//
//
//
//
//

public class ComprasValidation {
    public static void main(String[] args) {
  
    //  Compras compras = new Compra(1, "Teste nome", "Teste", "Teste", "Teste", "Teste", "Teste", "Teste");
  
    ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    Validator validator = factory.getValidator ();
  
    Set<ConstraintViolation<Compras>> constraintViolations =
    validator.validate( compras);
  
    for (ConstraintViolation error: constraintViolations) {
        String msgError = error.getMessage();
        System.out.println (msgError);
    }
  }
  }