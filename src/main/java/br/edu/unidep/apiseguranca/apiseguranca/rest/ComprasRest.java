package br.edu.unidep.apiseguranca.apiseguranca.rest;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;


import br.edu.unidep.apiseguranca.apiseguranca.model.Compras;
import br.edu.unidep.apiseguranca.apiseguranca.repository.ComprasRepository;

@RestController
@RequestMapping("/compras")
public class ComprasRest {
	
	@Autowired
	ComprasRepository repositorio;
	
	@GetMapping("/ok")
	public String ok() {
		return "ok";
	}

	@GetMapping
	public List<Compras> listar() {
		return repositorio.findAll();
	}
	
	@PostMapping
	public ResponseEntity<Compras> criar(@Valid @RequestBody Compras compras,
			HttpServletResponse response) {
		
		Compras comprasSalva = repositorio.save(compras);
		
		URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().
				path("/{compras_id}").buildAndExpand(
						comprasSalva.getCodigo()).toUri();
						
		response.setHeader("Location", uri.toASCIIString());
		
		return ResponseEntity.created(uri).body(comprasSalva);
		
	}
	
	@GetMapping("/{compras_id}")
	public ResponseEntity<Compras> buscarPeloCodigo(
			@PathVariable Long compras_id) {
		
		Optional<Compras> compras = repositorio.findById(compras_id);
		
		return ResponseEntity.ok(compras.get());
	}
	
	@DeleteMapping("/{compras_id}")
	public void remover(@PathVariable Long compras_id) {
		Optional<Compras> compras = repositorio.findById(compras_id);
		if (compras.isPresent()) {
			repositorio.deleteById(compras_id);
		}
	}
}
