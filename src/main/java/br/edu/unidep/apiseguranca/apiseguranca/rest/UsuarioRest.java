package br.edu.unidep.apiseguranca.apiseguranca.rest;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;


import br.edu.unidep.apiseguranca.apiseguranca.model.Usuario;
import br.edu.unidep.apiseguranca.apiseguranca.repository.UsuarioRepository;

@RestController
@RequestMapping("/usuarios")
public class UsuarioRest {
	
	@Autowired
	UsuarioRepository repositorio;
	
	@GetMapping("/ok")
	public String ok() {
		return "ok";
	}

	@GetMapping
	public List<Usuario> listar() {
		return repositorio.findAll();
	}
	
	@PostMapping
	public ResponseEntity<Usuario> criar(@Valid @RequestBody Usuario usuarios,
			HttpServletResponse response) {
		
		Usuario usuariosSalva = repositorio.save(usuarios);
		
		URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().
				path("/{usuarios_id}").buildAndExpand(
						usuariosSalva.getCodigo()).toUri();
						
		response.setHeader("Location", uri.toASCIIString());
		
		return ResponseEntity.created(uri).body(usuariosSalva);
		
	}
	
	@GetMapping("/{usuarios_id}")
	public ResponseEntity<Usuario> buscarPeloCodigo(
			@PathVariable Long usuarios_id) {
		
		Optional<Usuario> usuarios = repositorio.findById(usuarios_id);
		
		return ResponseEntity.ok(usuarios.get());
	}
	
	@DeleteMapping("/{usuarios_id}")
	public void remover(@PathVariable Long usuarios_id) {
		Optional<Usuario> usuarios = repositorio.findById(usuarios_id);
		if (usuarios.isPresent()) {
			repositorio.deleteById(usuarios_id);
		}
	}
}
