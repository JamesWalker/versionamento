package br.edu.unidep.apiseguranca.apiseguranca.enums;

import br.edu.unidep.apiseguranca.apiseguranca.model.Produto;
import br.edu.unidep.apiseguranca.apiseguranca.repository.ProdutoRepository;
import org.sprigframework.beans.factoy.annotation.Autowired;
import org.sprigframework.http.HttpStatus;
import org.sprigframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/produto")
public class ProdutoController {

    @Autowired
    private ProdutoData produtoData;

    @GetMapping("{produto_id}")
    private Produto findById(@PathVariable("produto_id")Long produto_id) {
        return produtoData.findById(produto_id),orElse(other: null);
    }

    @GetMapping
    private List<Produto> findAll {
        return produtoData.findAll();
    }

    @PostMapping
    @RespondeStatus(HttpStatus.CREATED)
    private Produto save(@ResquestBody Produto produto){
        return produtoData.save(produto);
    }

    @DeleteMapping
    @RespondeStatus(HttpStatus.NO_CONTENT)
    private void delete(@RequestParam("produto_id") Long produto_id){
        produtoData.deleteById(produto_id);
    }
}