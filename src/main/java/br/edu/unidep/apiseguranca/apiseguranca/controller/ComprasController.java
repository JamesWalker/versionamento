package br.edu.unidep.apiseguranca.apiseguranca.enums;

import br.edu.unidep.apiseguranca.apiseguranca.model.Compras;
import br.edu.unidep.apiseguranca.apiseguranca.repository.ComprasRepository;
import org.sprigframework.beans.factoy.annotation.Autowired;
import org.sprigframework.http.HttpStatus;
import org.sprigframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/compras")
public class ComprasController {

    @Autowired
    private ComprasData comprasData;

    @GetMapping("{compras_id}")
    private Compras findById(@PathVariable("compras_id")Long compras_id) {
        return comprasData.findById(compras_id),orElse(other: null);
    }

    @GetMapping
    private List<Compras> findAll {
        return comprasData.findAll();
    }

    @PostMapping
    @RespondeStatus(HttpStatus.CREATED)
    private Compras save(@ResquestBody Compras compras){
        return comprasData.save(compras);
    }

    @DeleteMapping
    @RespondeStatus(HttpStatus.NO_CONTENT)
    private void delete(@RequestParam("compras_id") Long compras_id){
        comprasData.deleteById(compras_id);
    }
}