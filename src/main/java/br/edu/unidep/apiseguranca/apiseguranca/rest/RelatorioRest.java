package br.edu.unidep.apiseguranca.apiseguranca.rest;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;


import br.edu.unidep.apiseguranca.apiseguranca.model.Relatorio;
import br.edu.unidep.apiseguranca.apiseguranca.repository.RelatorioRepository;

@RestController
@RequestMapping("/relatorio")
public class RelatorioRest {
	
	@Autowired
	RelatorioRepository repositorio;
	
	@GetMapping("/ok")
	public String ok() {
		return "ok";
	}

	@GetMapping
	public List<Relatorio> listar() {
		return repositorio.findAll();
	}
	
	@PostMapping
	public ResponseEntity<Relatorio> criar(@Valid @RequestBody Relatorio relatorio,
			HttpServletResponse response) {
		
		Relatorio relatorioSalva = repositorio.save(relatorio);
		
		URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().
				path("/{relatorio_id}").buildAndExpand(
						relatorioSalva.getCodigo()).toUri();
						
		response.setHeader("Location", uri.toASCIIString());
		
		return ResponseEntity.created(uri).body(relatorioSalva);
		
	}
	
	@GetMapping("/{relatorio_id}")
	public ResponseEntity<Relatorio> buscarPeloCodigo(
			@PathVariable Long relatorio_id) {
		
		Optional<Relatorio> relatorio = repositorio.findById(relatorio_id);
		
		return ResponseEntity.ok(relatorio.get());
	}
	
	@DeleteMapping("/{relatorio_id}")
	public void remover(@PathVariable Long relatorio_id) {
		Optional<Relatorio> relatorio = repositorio.findById(relatorio_id);
		if (relatorio.isPresent()) {
			repositorio.deleteById(relatorio_id);
		}
	}
}
