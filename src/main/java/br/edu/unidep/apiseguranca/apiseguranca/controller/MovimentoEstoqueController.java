package br.edu.unidep.apiseguranca.apiseguranca.enums;

import br.edu.unidep.apiseguranca.apiseguranca.model.MovimentoEstoque;
import br.edu.unidep.apiseguranca.apiseguranca.repository.MovimentoEstoqueRepository;
import org.sprigframework.beans.factoy.annotation.Autowired;
import org.sprigframework.http.HttpStatus;
import org.sprigframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/movimentoestoque")
public class MovimentoEstoqueController {

    @Autowired
    private MovimentoEstoqueData movimentoestoqueData;

    @GetMapping("{movimentoestoque_id}")
    private MovimentoEstoque findById(@PathVariable("movimentoestoque_id")Long movimentoestoque_id) {
        return movimentoestoqueData.findById(movimentoestoque_id),orElse(other: null);
    }

    @GetMapping
    private List<MovimentoEstoque> findAll {
        return movimentoestoqueData.findAll();
    }

    @PostMapping
    @RespondeStatus(HttpStatus.CREATED)
    private MovimentoEstoque save(@ResquestBody MovimentoEstoque movimentoestoque){
        return movimentoestoqueData.save(movimentoestoque);
    }

    @DeleteMapping
    @RespondeStatus(HttpStatus.NO_CONTENT)
    private void delete(@RequestParam("movimentoestoque_id") Long movimentoestoque_id){
        movimentoestoqueData.deleteById(movimentoestoque_id);
    }
}