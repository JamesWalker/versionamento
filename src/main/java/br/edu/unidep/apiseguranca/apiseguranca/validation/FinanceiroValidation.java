//
//
//
//
//
//
//

public class FinanceiroValidation {
    public static void main(String[] args) {
  
    //  Financeiro financeiro = new Compra(1, "Teste nome", "Teste", "Teste", "Teste", "Teste", "Teste", "Teste");
  
    ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    Validator validator = factory.getValidator ();
  
    Set<ConstraintViolation<Financeiro>> constraintViolations =
    validator.validate( financeiro);
  
    for (ConstraintViolation error: constraintViolations) {
        String msgError = error.getMessage();
        System.out.println (msgError);
    }
  }
  }