//
//
//
//
//

public class FornecedorValidation {
    public static void main(String[] args) {
  
    //  Fornecedor fornecedor = new Compra(1, "Teste nome", "Teste", "Teste", "Teste", "Teste", "Teste", "Teste");
  
    ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    Validator validator = factory.getValidator ();
  
    Set<ConstraintViolation<Fornecedor>> constraintViolations =
    validator.validate( fornecedor);
  
    for (ConstraintViolation error: constraintViolations) {
        String msgError = error.getMessage();
        System.out.println (msgError);
    }
  }
  }