package br.edu.unidep.apiseguranca.apiseguranca.rest;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;


import br.edu.unidep.apiseguranca.apiseguranca.model.Financeiro;
import br.edu.unidep.apiseguranca.apiseguranca.repository.FinanceiroRepository;

@RestController
@RequestMapping("/financeiro")
public class FinanceiroRest {
	
	@Autowired
	FinanceiroRepository repositorio;
	
	@GetMapping("/ok")
	public String ok() {
		return "ok";
	}

	@GetMapping
	public List<Financeiro> listar() {
		return repositorio.findAll();
	}
	
	@PostMapping
	public ResponseEntity<Financeiro> criar(@Valid @RequestBody Financeiro financeiro,
			HttpServletResponse response) {
		
		Financeiro financeiroSalva = repositorio.save(financeiro);
		
		URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().
				path("/{financeiro_id}").buildAndExpand(
						financeiroSalva.getCodigo()).toUri();
						
		response.setHeader("Location", uri.toASCIIString());
		
		return ResponseEntity.created(uri).body(financeiroSalva);
		
	}
	
	@GetMapping("/{financeiro_id}")
	public ResponseEntity<Financeiro> buscarPeloCodigo(
			@PathVariable Long financeiro_id) {
		
		Optional<Financeiro> financeiro = repositorio.findById(financeiro_id);
		
		return ResponseEntity.ok(financeiro.get());
	}
	
	@DeleteMapping("/{financeiro_id}")
	public void remover(@PathVariable Long financeiro_id) {
		Optional<Financeiro> financeiro = repositorio.findById(financeiro_id);
		if (financeiro.isPresent()) {
			repositorio.deleteById(financeiro_id);
		}
	}
}
