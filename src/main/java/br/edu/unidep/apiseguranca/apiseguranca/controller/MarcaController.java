package br.edu.unidep.apiseguranca.apiseguranca.enums;

import br.edu.unidep.apiseguranca.apiseguranca.model.Marca;
import br.edu.unidep.apiseguranca.apiseguranca.repository.MarcaRepository;
import org.sprigframework.beans.factoy.annotation.Autowired;
import org.sprigframework.http.HttpStatus;
import org.sprigframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/marca")
public class MarcaController {

    @Autowired
    private MarcaData marcaData;

    @GetMapping("{marca_id}")
    private Marca findById(@PathVariable("marca_id")Long marca_id) {
        return marcaData.findById(marca_id),orElse(other: null);
    }

    @GetMapping
    private List<Marca> findAll {
        return marcaData.findAll();
    }

    @PostMapping
    @RespondeStatus(HttpStatus.CREATED)
    private Marca save(@ResquestBody Marca marca){
        return marcaData.save(marca);
    }

    @DeleteMapping
    @RespondeStatus(HttpStatus.NO_CONTENT)
    private void delete(@RequestParam("marca_id") Long marca_id){
        marcaData.deleteById(marca_id);
    }
}