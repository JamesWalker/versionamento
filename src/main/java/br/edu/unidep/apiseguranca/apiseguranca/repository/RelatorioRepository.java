package br.edu.unidep.apiseguranca.apiseguranca.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.edu.unidep.apiseguranca.apiseguranca.model.Relatorio;

public interface RelatorioRepository extends JpaRepository<Relatorio, Long> {

}
