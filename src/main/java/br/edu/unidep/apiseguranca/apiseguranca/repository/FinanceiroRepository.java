package br.edu.unidep.apiseguranca.apiseguranca.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.edu.unidep.apiseguranca.apiseguranca.model.Financeiro;

public interface FinanceiroRepository extends JpaRepository<Financeiro, Long> {

}
