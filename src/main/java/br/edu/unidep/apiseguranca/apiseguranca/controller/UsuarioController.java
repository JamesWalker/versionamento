package br.edu.unidep.apiseguranca.apiseguranca.enums;

import br.edu.unidep.apiseguranca.apiseguranca.model.Usuario;
import br.edu.unidep.apiseguranca.apiseguranca.repository.UsuarioRepository;
import org.sprigframework.beans.factoy.annotation.Autowired;
import org.sprigframework.http.HttpStatus;
import org.sprigframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/usuario")
public class UsuarioController {

    @Autowired
    private UsuarioData usuarioData;

    @GetMapping("{usuario_id}")
    private Usuario findById(@PathVariable("usuario_id")Long usuario_id) {
        return usuarioData.findById(usuario_id),orElse(other: null);
    }

    @GetMapping
    private List<Usuario> findAll {
        return usuarioData.findAll();
    }

    @PostMapping
    @RespondeStatus(HttpStatus.CREATED)
    private Usuario save(@ResquestBody Usuario usuario){
        return usuarioData.save(usuario);
    }

    @DeleteMapping
    @RespondeStatus(HttpStatus.NO_CONTENT)
    private void delete(@RequestParam("usuario_id") Long usuario_id){
        usuarioData.deleteById(usuario_id);
    }
}