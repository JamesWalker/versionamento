package br.edu.unidep.apiseguranca.apiseguranca.rest;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;


import br.edu.unidep.apiseguranca.apiseguranca.model.Fornecedor;
import br.edu.unidep.apiseguranca.apiseguranca.repository.FornecedorRepository;

@RestController
@RequestMapping("/fornecedor")
public class FornecedorRest {
	
	@Autowired
	FornecedorRepository repositorio;
	
	@GetMapping("/ok")
	public String ok() {
		return "ok";
	}

	@GetMapping
	public List<Fornecedor> listar() {
		return repositorio.findAll();
	}
	
	@PostMapping
	public ResponseEntity<Fornecedor> criar(@Valid @RequestBody Fornecedor fornecedor,
			HttpServletResponse response) {
		
		Fornecedor fornecedorSalva = repositorio.save(fornecedor);
		
		URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().
				path("/{fornecedor_id}").buildAndExpand(
						fornecedorSalva.getCodigo()).toUri();
						
		response.setHeader("Location", uri.toASCIIString());
		
		return ResponseEntity.created(uri).body(fornecedorSalva);
		
	}
	
	@GetMapping("/{fornecedor_id}")
	public ResponseEntity<Fornecedor> buscarPeloCodigo(
			@PathVariable Long fornecedor_id) {
		
		Optional<Fornecedor> fornecedor = repositorio.findById(fornecedor_id);
		
		return ResponseEntity.ok(fornecedor.get());
	}
	
	@DeleteMapping("/{fornecedor_id}")
	public void remover(@PathVariable Long fornecedor_id) {
		Optional<Fornecedor> fornecedor = repositorio.findById(fornecedor_id);
		if (fornecedor.isPresent()) {
			repositorio.deleteById(fornecedor_id);
		}
	}
}
