package br.edu.unidep.apiseguranca.apiseguranca.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;
import org.hibernate.*;
import br.edu.unidep.apiseguranca.apiseguranca.model.Marca;
import br.edu.unidep.apiseguranca.apiseguranca.model.Fornecedor;

@Entity
@Table(name = "produto")
public class Produto {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "produto_id")
	private Long produto_id;

	@OneToOne
	@JoinTable(name = 'marca', joinColumns={@JoinColumn(name = "produto_id", referencedColumnName="produto_id")})
	private List<Marca> nome;

	@ManyToMany
	@JoinTable(name = 'fornecedor', joinColumns={@JoinColumn(name = "fornecedor_id", referencedColumnName="fornecedor_id")})
	private List<Fornecedor> nome;
	
	@NotNull
	@Size(min = 5, max = 50)
	@Column(name = "nome")
	private String nome;

	@Size(min = 5, max = 50)
	@Column(name = "campo_1")
	private String campo_1;
	
	@Size(min = 5, max = 50)
	@Column(name = "campo_2")
	private String campo_2;

	@Size(min = 5, max = 50)
	@Column(name = "campo_3")
	private String campo_3;

	@Size(min = 5, max = 50)
	@Column(name = "campo_4")
	private String campo_4;

	@Size(min = 5, max = 50)
	@Column(name = "campo_5")
	private String campo_5;
	
	
	
	public Long getCodigo() {
		return produto_id;
	}

	public void setCodigo(Long produto_id) {
		this.produto_id = produto_id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCampo1() {
		return campo_1;
	}

	public void setCampo1(String campo_1) {
		this.campo_1 = campo_1;
	}

	public String getCampo2() {
		return campo_2;
	}

	public void setCampo2(String campo_2) {
		this.campo_2 = campo_2;
	}

	public String getCampo3() {
		return campo_3;
	}

	public void setCampo3(String campo_3) {
		this.campo_3 = campo_3;
	}

	public String getCampo4() {
		return campo_4;
	}

	public void setCampo4(String campo_4) {
		this.campo_4 = campo_4;
	}
	public String getCampo5() {
		return campo_5;
	}

	public void setCampo5(String campo_5) {
		this.campo_5 = campo_5;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((produto_id == null) ? 0 : produto_id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Produto other = (Produto) obj;
		if (produto_id == null) {
			if (other.produto_id != null)
				return false;
		} else if (!produto_id.equals(other.produto_id))
			return false;
		return true;
	}
	
}
